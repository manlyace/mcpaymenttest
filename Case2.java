package com.mcp.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Case2 {

    public static void main(String[] args) {
        int arr[] = {1,2,3,4};
        int x = 4;
        List<Integer> result = new ArrayList<>();
        for(int i=0; i<arr.length; i++) {
            for(int j=0; j<arr.length; j++) {
                if(arr[i]/arr[j] != x && !result.contains(arr[i])) {
                    result.add(arr[i]);
                    if(result.contains(x)) {
                        result.removeAll(Arrays.asList(x));
                    }
                }
            }
        }
        System.out.println(result);

    }
}
