package com.mcp.test;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Case1 {

    public static void main(String[] args) {

        int nums[] = {3,1,2,4};
        int a = IntStream.of(nums).max().getAsInt();
        System.out.println(a);

        int arr[] = {10, 324, 45, 90, 89,91};
        int max = Arrays.stream(arr).max().getAsInt();
        System.out.println(max);
    }
}
