package com.mcp.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Case3 {

    public static void main(String[] args) {
        String word = "souvenir loud four lost";
        int x = 4;
        List<String> listOfWord = new ArrayList<>();
        listOfWord = Arrays.asList(word.split(" "));
        List<String> result = new ArrayList<>();
        for(String data : listOfWord) {
            if(data.length() == x) {
                result.add(data);
            }
        }
        System.out.println(result);
    }
}
